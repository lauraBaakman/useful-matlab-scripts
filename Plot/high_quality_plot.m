function high_quality_plot( varargin )
    
    global function_name;
    function_name = 'high_quality_plot';
    
    parser = initialize_parser();
    
    set_parameters(parser);
    
    parse(parser, varargin{:});
    
    results = parser.Results;
    
    % Axis
    set(get(gca, 'title') , 'FontSize', results.FontSize, 'FontWeight', 'Normal', 'FontName', results.FontName);
    set(get(gca, 'xlabel'), 'FontSize', results.FontSize, 'FontWeight', 'Normal', 'FontName', results.FontName);
    set(get(gca, 'ylabel'), 'FontSize', results.FontSize, 'FontWeight', 'Normal', 'FontName', results.FontName);
    
    set(gca,'FontName', results.FontName);
    
    set(gca, 'LineWidth', 1, ...
        'FontSize', results.FontSize, ...
        'FontWeight', 'Normal',...
        'FontName', results.FontName);
    
    % Legend
    set(legend, 'FontSize', results.FontSize, 'FontName', results.FontName);
    set(legend, 'FontWeight', 'Normal', 'FontName', results.FontName);
    legend boxoff;
    
    % Figure
    set(findall(gcf,'type','text'),'FontName', results.FontName);
    
    [paper_width, paper_height, margin] = calculate_paper_size(results);
    paper_position = calculate_paper_position(paper_width, paper_height, margin);
    
    set(gcf, 'color', 'w', ...
        'PaperUnits', 'points', ...
        'PaperSize', [paper_width, paper_height], ...
        'PaperPosition', paper_position);
    set(gcf, 'PaperPositionMode', 'Manual');
    
    global print_options;
    if ~strcmp(results.Save, '')
        print(gcf, '-painters', sprintf('-d%s', print_options.ext), sprintf('-r%d', results.Dpi), print_options.path);
    end
end

function [width, height, margin] = calculate_paper_size(results)
    width = results.PaperWidthRatio * results.PaperSize;
    height = width * results.PaperWidthHeightRatio;
    margin = results.Margin;
end

function position = calculate_paper_position(paper_width, paper_height, margin)
    position = [margin, margin, paper_width - 2 * margin, paper_height - 2 * margin];
end

function parser = initialize_parser()
    global function_name;
    
    parser = inputParser;
    
    % Interpret structure as separate input.
    parser.StructExpand = false;
    parser.CaseSensitive = false;
    
    % Keep any unmatched variables.
    parser.KeepUnmatched = false;
    
    % Don't accept any partial matched variable names (with respect to
    % the CaseSensitive property).
    parser.PartialMatching = false;
    
    % Function name is used to set appropriate error message.
    parser.FunctionName = function_name;
    
    set_defaults(); set_valid_values();
end

function set_defaults ()
    global default;
    default = struct(...
        'save', '', ...
        'dpi', 150, ...
        'ext', '.pdf', ...
        'paper_size', 400, ...
        'paper_width_ratio', 1.0, ...
        'paper_width_height_ratio', 1.0, ...
        'margin', 1, ...
        'font_size', 11, ...
        'FontName', 'NewCenturySchoolBook' ...
        );
end

function set_valid_values()
    global valid;
    valid = struct(...
        'ext', {{'.pdf', '.png', '.jpeg'}},...
        'dpi', [0, 150, 300], ...
        'font_names',  {{'AvantGarde'; 'Bookman'; 'Courier'; 'Helvetica'; ...
        'Helvetica-Narrow'; 'NewCenturySchoolBook'; 'Palatino'; ...
        'Symbol'; 'Times'; 'ZapfChancery'; 'ZapfDingbats'}}...
        );
end

function validate_string(input)
    global function_name;
    validateattributes(input, {'char'}, {'nonempty'}, function_name);
end

function validate_number(input)
    global function_name;
    validateattributes(input, {'numeric'}, {'nonempty'}, function_name);
end

function set_print_options(path, filename, ext)
    global print_options;
    print_options = struct(...
        'path', sprintf('%s/%s%s', path, filename, ext), ...
        'ext', ext(2:end)...
    ); 
end

function tf = validate_save(input)
    global default valid;
    validate_string(input); tf = false;
    
    [path, filename, ext] = fileparts(input);
    
    if isempty(ext)
        ext = default.ext;
    end
    
    if ~exist(path, 'dir')
        error('Expected input to be a valid path.')
    elseif isempty(filename)
        error('Expected input to have a valid filename')
    elseif ~ismember(ext, valid.ext)
        expected_values = sprintf('%s ', valid.ext{:});
        error('Expected input to be one of these values: \n\n\t%s \n\nInstead its value was %s', expected_values, input);
    else
        set_print_options(path, filename, ext);
        tf = true;
    end
end

function tf = validate_dpi(input)
    global valid;
    validate_number(input); tf = false;
    if ~ismember(input, valid.dpi)
        expected_values = sprintf('%d ', valid.dpi);
        error('Expected input to be one of these values: \n\n\t%s \n\nInstead its value was %d.', expected_values, input);
    else
        tf = true;
    end
end

function tf = validate_font(input)
    global valid;
    validate_string(input); tf = false;
    if ~ismember(input, valid.font_names)
        expected_values = sprintf('%s ', valid.font_names{:});
        error('Expected input to be one of these values: \n\n\t%s \n\nInstead its value was %s.', expected_values, input);
    else
        tf = true;
    end
end

function set_parameters(parser)
    global default;
    addParameter(parser, 'Save', default.save, @validate_save);
    addParameter(parser, 'Dpi', default.dpi, @validate_dpi);
    addParameter(parser, 'PaperSize', default.paper_size, @isnumeric);
    addParameter(parser, 'PaperWidthRatio', default.paper_width_ratio, @isnumeric);
    addParameter(parser, 'PaperWidthHeightRatio', default.paper_width_height_ratio, @isnumeric);
    addParameter(parser, 'Margin', default.margin, @isnumeric);
    addParameter(parser, 'FontSize', default.font_size, @isnumeric);
    addParameter(parser, 'FontName', default.FontName, @validate_font);
    
    % Figures: http://nl.mathworks.com/help/matlab/ref/figure-properties.html
    % Add: PaperType: {'a4paper', 'twocolumn', 'etc'} with default width
    % attached -> if PaperType is specified PaperWidth should not be given.
    
    % Add: PaperHeight: Maybe somebody wants to specify this also.
    
    % Add: Renderer: {'opengl', 'painters'} opengl is better in 3D images,
    % painters for 2D.
   
    % Axis: http://nl.mathworks.com/help/matlab/ref/axes-properties.html
end


